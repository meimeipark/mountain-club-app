import 'package:flutter/material.dart';

class PageIndex extends StatefulWidget {
  const PageIndex({super.key});

  @override
  State<PageIndex> createState() => _PageIndexState();
}

class _PageIndexState extends State<PageIndex> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          '등산 사랑회',
          style: TextStyle(
            fontWeight: FontWeight.w700,
            fontSize: 30,
          ),
        ),
        backgroundColor: Colors.lightGreen,
        leading: Icon(Icons.menu),
        centerTitle: true,
        elevation: 0.0,
        actions: [
          IconButton(
            onPressed: () {},
            icon: Icon(Icons.person),
          ),
        ],
      ),
      drawer: Drawer(
        child: ListView(
          children: [
            UserAccountsDrawerHeader(
              currentAccountPicture: CircleAvatar(
                backgroundImage: AssetImage('assets/dabi3'),
              ),
              accountName: Text('김다비'),
              accountEmail: Text('dabiboss@gmail.com'),
              onDetailsPressed: () {},
              decoration: BoxDecoration(
                color: Colors.lightGreen,
              ),
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              child: Text(
                "동호회 소개",
                style: TextStyle(
                  fontWeight: FontWeight.w800,
                  fontSize: 26,
                ),
              ),
              margin: EdgeInsets.all(20),
            ),
            Image.asset(
                'assets/mountain.jpg',
              width: 500,
              height: 350,
              fit: BoxFit.fill,
            ),
            Container(
              child: Column(
                children: [
                  Text(
                    '등산 사랑회',
                    style: TextStyle(
                      letterSpacing: 1.0,
                      fontSize: 23,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Container(
                    child: Text(
                      '회비 : 10,000 \n매주 토요일 새벽 6시 출발',
                      style: TextStyle(
                        fontSize: 18,
                        letterSpacing: 1.2,
                        color: Colors.grey,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
                  ),
                ],
              ),
              padding: EdgeInsets.fromLTRB(30, 20, 30, 20),
            ),
          Container(
            width: 800,
            decoration: BoxDecoration(
                border:Border(
                  bottom: BorderSide(color: Colors.black, width: 1),
                ),
            ),
            margin: EdgeInsets.fromLTRB(0, 0, 0, 30),
          ),
          Text(
              '등산 멤버',
            style: TextStyle(
              fontSize: 23,
              letterSpacing: 1.2,
              color: Colors.grey,
              fontWeight: FontWeight.bold,
            ),
          ),
          Container(
            child: Row(
              children: [
                Container(
                  child: Column(
                    children: [
                      Image.asset(
                        'dabi2.jpg',
                        width: 180,
                        height: 230,
                        fit: BoxFit.fill,
                      ),
                      Text('회장 김다비',
                        style: TextStyle(
                            letterSpacing: 1.0,
                            fontSize: 17,
                            fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                  margin: EdgeInsets.fromLTRB(30, 30, 20, 10),
                ),
                Container(
                  child: Column(
                    children: [
                      Image.asset(
                        'member1.jpg',
                        width: 180,
                        height: 230,
                        fit: BoxFit.fill,
                      ),
                      Text('회원 유승호',
                        style: TextStyle(
                            letterSpacing: 1.0,
                            fontSize: 17,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                  margin: EdgeInsets.fromLTRB(0, 30, 20, 10),
                ),
                Container(
                  child: Column(
                    children: [
                      Image.asset(
                        'member2.jpg',
                        width: 180,
                        height: 230,
                        fit: BoxFit.fill,
                      ),
                      Text('회원 차은우',
                        style: TextStyle(
                            letterSpacing: 1.0,
                            fontSize: 17,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                  margin: EdgeInsets.fromLTRB(0, 30, 20, 10),
                ),
                Container(
                  child: Column(
                    children: [
                      Image.asset(
                        'member3.jpg',
                        width: 180,
                        height: 230,
                        fit: BoxFit.fill,
                      ),
                      Text('회원 서강준',
                        style: TextStyle(
                            letterSpacing: 1.0,
                            fontSize: 17,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                  margin: EdgeInsets.fromLTRB(0, 30, 20, 10),
                ),
              ],
            ),
            margin: EdgeInsets.fromLTRB(18, 10, 18, 20),
          ),
          ],
        ),
      ),
    );
  }
}
